package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type Temperature struct {
	Id string `json:"Id"`
	Label string `json:"Label"`
	SerialNo string `json:"SerialNo"`
	// Apparently they spelled Temperature wrong...
	Temperature string `json:"Temprature"`
	DeviceId string `json:"DeviceId"`
}

func GetTemperatures() []*Temperature {
	payload := fmt.Sprintf(`{"id":"%s", "Version": "%s"}`, os.Getenv("SA_SITE_ID"), CurrentSession.RawVersion)
	data, err := CurrentSession.postRequest("/Panel/GetTempratures/", payload)
	if err != nil {
		log.Fatal(err)
	}
	var structData []*Temperature
	_ = json.Unmarshal([]byte(data), &structData)
	return structData
}
