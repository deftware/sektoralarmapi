package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type SmartPlug struct {
	Consumption string `json:"Consumption"`
	DisplayScenarios bool `json:"DisplayScenarios"`
	Id string `json:"Id"`
	Label string `json:"Label"`
	PanelId string `json:"PanelId"`
	SerialNo string `json:"SerialNo"`
	Scenarios string `json:"Scenarios"`
	Status string `json:"Status"`
	TimerActive bool `json:"TimerActive"`
	TimerEvents string `json:"TimerEvents"`
	TimerEventsSchedule string `json:"TimerEventsSchedule"`
}

func (s *SmartPlug) IsEnabled() bool {
	return s.Status == "On"
}

func (s *SmartPlug) ToggleState() bool {
	if s.Status == "On" {
		s.Status = "Off"
	} else {
		s.Status = "On"
	}
	_ = s.SetState(s.Status == "On")
	return s.Status == "On"
}

func (s *SmartPlug) SetState(state bool) string {
	stringState := "On"
	if !state {
		stringState = "Off"
	}
	s.Status = stringState
	payload := fmt.Sprintf(`{"id":"%s", "SwitchStatus": "%s", "SwitchId": "%s"}`, os.Getenv("SA_SITE_ID"), stringState, s.Id)
	data, err := CurrentSession.postRequest("/SmartPlugs/SwitchSmartPlug", payload)
	if err != nil {
		log.Fatal(err)
	}
	return data
}

func GetSmartPlugs() []*SmartPlug {
	data, err := CurrentSession.getRequest(fmt.Sprintf("/SmartPlugs/GetSmartPlugsForPanel?id=%s&WithStatus=%t",  os.Getenv("SA_SITE_ID"), true))
	if err != nil {
		log.Fatal(err)
	}
	var structData []*SmartPlug
	_ = json.Unmarshal([]byte(data), &structData)
	return structData
}
