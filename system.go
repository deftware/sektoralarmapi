package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type PanelData struct {
	PartialAvalible bool `json:"PartialAvalible"`
	PanelQuickArm bool `json:"PanelQuickArm"`
	PanelCodeLength int `json:"PanelCodeLength"`
	LockLanguage int `json:"LockLanguage"`
	SupportsApp bool `json:"SupportsApp"`
	SupportsInterviewServices bool `json:"SupportsInterviewServices"`
	SupportsPanelUsers bool `json:"SupportsPanelUsers"`
	SupportsTemporaryPanelUsers bool `json:"SupportsTemporaryPanelUsers"`
	SupportsRegisterDevices bool `json:"SupportsRegisterDevices"`
	CanAddDoorLock bool `json:"CanAddDoorLock"`
	CanAddSmartPlug bool `json:"CanAddSmartPlug"`
	Wifi struct {
		WifiExist bool `json:"WifiExist"`
		Serial string `json:"Serial"`
	} `json:"Wifi"`
	PanelId string `json:"PanelId"`
	ArmedStatus string `json:"ArmedStatus"`
	PanelDisplayName string `json:"PanelDisplayName"`
	StatusAnnex string `json:"StatusAnnex"`
	PanelTime string `json:"PanelTime"`
	AnnexAvalible bool `json:"AnnexAvalible"`
	IVDisplayStatus bool `json:"IVDisplayStatus"`
	DisplayWizard bool `json:"DisplayWizard"`
	BookedStartDate string `json:"BookedStartDate"`
	BookedEndDate string `json:"BookedEndDate"`
	InstallationStatus int `json:"InstallationStatus"`
	InstallationAddress string `json:"InstallationAddress"`
	WizardStep int `json:"WizardStep"`
	AccessGroup int `json:"AccessGroup"`
	SessionExpires string `json:"SessionExpires"`
	IsOnline bool `json:"IsOnline"`
}

type AlarmSystem struct {
	Panel PanelData `json:"Panel"`
	Access []string `json:"Access"`
	Smartplugs []*SmartPlug `json:"Smartplugs"`
	Temperatures []*Temperature `json:"Temperatures"`
}

type AlarmHistory struct {
	Status string `json:"Status"`
	Message string `json:"Message"`
	LogDetails []struct {
		Time string `json:"Time"`
		EventType string `json:"EventType"`
		User string `json:"User"`
		Channel string `json:"Channel"`
		Keypad string `json:"Keypad"`
		LockName string `json:"LockName"`
	} `json:"LogDetails"`
}

func GetHistory() *AlarmHistory {
	data, err := CurrentSession.getRequest(fmt.Sprintf("/Panel/GetPanelHistory/%s",  os.Getenv("SA_SITE_ID")))
	if err != nil {
		log.Fatal(err)
	}
	var structData *AlarmHistory
	_ = json.Unmarshal([]byte(data), &structData)
	return structData
}

func GetSystem() *AlarmSystem {
	payload := fmt.Sprintf(`{"id":"%s", "Version": "%s"}`, os.Getenv("SA_SITE_ID"), CurrentSession.RawVersion)
	data, err := CurrentSession.postRequest("/Panel/GetOverview/", payload)
	if err != nil {
		log.Fatal(err)
	}
	var structData *AlarmSystem
	_ = json.Unmarshal([]byte(data), &structData)
	return structData
}
