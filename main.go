package main

import (
	"flag"
	"log"
	"os"
	"sync"
)

var (
	SmartPlugs    []*SmartPlug
	Temperatures  []*Temperature
	System        *AlarmSystem
	httpServerPtr = flag.String("http", "", "--http <ip bind>")
	plugsPtr      = flag.Bool("plugs", false, "--plugs")
	WaitGroup     sync.WaitGroup
)

func main() {
	flag.Parse()
	if len(os.Getenv("SA_EMAIL")) == 0 || len(os.Getenv("SA_PASSWORD")) == 0 || len(os.Getenv("SA_SITE_ID")) == 0 {
		log.Fatal("Please set the SA_EMAIL, SA_PASSWORD, and SA_SITE_ID environment variables!")
		return
	}
	Setup()
	if *httpServerPtr != "" {
		startServer(*httpServerPtr)
	}
	WaitGroup.Wait()
}

func Setup() {
	Login()
	WaitGroup.Add(3)
	go func() {
		Temperatures = GetTemperatures()
		log.Printf("Found %d temperature sensor(s)\n", len(Temperatures))
		for _, v := range Temperatures {
			log.Printf("Temperature in %s is currently %sc\n", v.Label, v.Temperature)
		}
		WaitGroup.Done()
	}()
	go func() {
		System = GetSystem()
		WaitGroup.Done()
	}()
	go func() {
		SmartPlugs = GetSmartPlugs()
		log.Printf("Found %d smartplug(s)\n", len(SmartPlugs))
		if *plugsPtr {
			WaitGroup.Add(len(SmartPlugs))
			for _, v := range SmartPlugs {
				go func() {
					log.Printf("Toggling smartplug %s to %t\n", v.Label, v.ToggleState())
					WaitGroup.Done()
				}()
			}
		}
		WaitGroup.Done()
	}()
}
